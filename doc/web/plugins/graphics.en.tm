<TeXmacs|1.0.7.2>

<style|tmweb>

<\body>
  <tmweb-current|Plug-ins|Graphics><tmweb-title|Visualization software and
  <TeXmacs>|<tmweb-plugin-links>>

  <section|Asymptote><label|asymptote>

  <tmdoc-include|plugins/asymptote/asymptote-web.en.tm>

  <section|Eukleides><label|eikleides>

  <tmdoc-include|plugins/eukleides/eukleides-web.en.tm>

  <section|Ghostscript><label|ghostscript>

  <tmdoc-include|plugins/ghostscript/ghostscript-web.en.tm>

  <section|TeXgraph><label|texgraph>

  <tmdoc-include|plugins/texgraph/texgraph-web.en.tm>

  <section|Xfig><label|xfig>

  <tmdoc-include|plugins/xfig/xfig-web.en.tm>

  <tmdoc-copyright|1999--2003|Joris van der Hoeven>

  <tmweb-license>
</body>

<\initial>
  <\collection>
    <associate|language|english>
    <associate|preamble|false>
  </collection>
</initial>