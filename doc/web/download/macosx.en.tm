<TeXmacs|1.0.7.5>

<style|tmweb>

<\body>
  <tmweb-current|Download|MacOS><tmweb-title|Installing <TeXmacs> on
  MacOS-X|<tmweb-download-links>>

  In order to install <TeXmacs> under <name|MacOS-X>, you have two options:

  <\enumerate>
    <item>Try <hlink|<name|Qt>-<TeXmacs>|#install>, our experimental
    <name|Qt> port of <TeXmacs> with an easy to install diskimage.

    <item>Use a more <hlink|stable version of <TeXmacs>|fink.en.tm>, based on
    <name|Fink>.
  </enumerate>

  <section|Installation of <name|Qt>-<TeXmacs>><label|install>

  In order to install the <name|Qt>-based version of <TeXmacs> on Windows,
  you should:

  <\enumerate>
    <item>Download the diskimage <hlink|<scm|TeXmacs.dmg>|ftp://ftp.texmacs.org/pub/TeXmacs/macosx/qt/TeXmacs.dmg>
    (17Mb, Intel processors only).

    <item>Open the diskimage and drag the <TeXmacs> icon to your applications
    folder.

    <item>Launch <TeXmacs> from your applications folder.
  </enumerate>

  <tmdoc-copyright|1999--2010|Massimiliano Gubinelli|Joris van der Hoeven>

  <tmweb-license>
</body>

<\initial>
  <\collection>
    <associate|language|english>
  </collection>
</initial>